#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use rocket::{State};
use rocket::request::{FromRequest, Request};
use rocket::data::Outcome;
use rocket::response::status;
use std::io::Cursor;

use rocket::response::{self, Response, Responder};
use serde_json;

use dbserver::db::db_record::{Record, TapeInfo, Tape, TapeElement};
use std::sync::Mutex;
use std::collections::BTreeMap;
use async_trait::async_trait;
use std::ops::Deref;

// use rocket::serde::json::{Json, JsonValue};
// use rocket::

struct Item<'r>(&'r Mutex<BTreeMap<u32, Record>>);

#[async_trait]
impl<'r> FromRequest<'r> for Item<'r> {
    type Error = ();
    async fn from_request(req: &'r Request<'_>) -> rocket::request::Outcome<Self, ()> {
        let outcome = req.guard::<&State<Mutex<BTreeMap<u32, Record>>>>().await
            .map(|s| Item(&s));
        outcome
    }
}

fn try_index(element: Option<&TapeElement>) -> String{
    match element {
        Some(v) => {
            serde_json::to_string(v).unwrap()
        }
        None => {
            String::from("Index does not exists")
        }
    }
}

#[get("/")]
fn index() -> String {
    format!("Hello")
}

// TODO: Make the response right
#[post("/api/insert/<field>/<io_id>/<index>/<id>", data="<payload>")]
async fn insert_item(field: &str, io_id: u32, index: usize, id: u32, payload: String,
               store: &State<Mutex<BTreeMap<u32, Record>>>) -> status::Accepted<String> {
    let user_store = &mut store.lock().unwrap();
    // let user_id = id.parse::<u32>().unwrap();
    match field {
        "new_user" => {
            let record = Record::new(&payload);
            user_store.insert(id, record);
            // content::Json(&String::from("{'status': 'ok', 'message': 'User added'}"))
            status::Accepted(Some(format!("{}",
                                String::from("{'status': 'ok', 'message': 'user added'}"))))
        }
        "new_tape" => {
            let mut record = user_store.get(&id).unwrap().clone();
            // make a new tape obj from the payload
            let new_tape = Tape::new(&payload);
            record.push_tape(new_tape);
            user_store.insert(id, record);
            status::Accepted(Some(format!("{}",
                                  String::from("{'status': 'ok', 'message': 'tape added'}"))))

        }
        "new_data" => {
            // find the index of the io we want to update
            // let record = user_store.get_mut(&id).unwrap();
            // println!("{}", "new data called");
            let tapes = user_store.get_mut(&id).unwrap().tapes.clone();
            let mut count = 0;
            let mut record = user_store.get(&id).unwrap().clone();
            for t in tapes {
                // println!("{}", t.tape_info.io_id);
                if t.tape_info.io_id == io_id {
                    // println!("{}", "inside if");
                    // user_store.get_mut(&id).unwrap().tapes[count].push_data(&payload);
                    let x = record.tapes[count].push_data(&payload);
                    user_store.insert(id, record.to_owned());
                }
                count += 1;
            }
            status::Accepted(Some(format!("{}",
                                  String::from("{'status': 'ok', 'message': 'data added'}"))))
            // content::Json(&String::from("{'status': 'ok', 'message': 'answer added'}"))
        }
        _ => {
            println!("Not Implemented");
            // content::Json(&String::from("{'status': 'fail', 'message': 'Not implemented'}"))
            status::Accepted(Some(format!("{}",
                          String::from("{'status': 'fail', 'message': 'Not Implemented'}"))))
        }
    }
}

#[get("/api/get/<field>/<io_id>/<index>/<id>")]
async fn get_item(field: &str, io_id: u32, index: usize, id: u32,
            store: &State<Mutex<BTreeMap<u32, Record>>>) -> status::Accepted<String> {
    // let user_id = &id.parse::<u32>().unwrap();
    let user_store = store.lock().unwrap();
    let record = user_store.get(&id).unwrap();
    let ta = record.tapes.len();
    println!("tape len: {}", ta);
    let da = record.tapes[0].tape_data.len();
    println!("data len: {}", da);
    let mut element_json: String = String::new();
    match field {
        "data" => {
            // let element_json = &mut String::from("");
            // find the io using the id
            for t in &record.tapes {
                if t.tape_info.io_id == io_id {
                    let element = t.tape_data.get(index);
                    element_json = try_index(element);
                    //elem.push(element_json)
                }
                else {
                    element_json = String::from("Error: IO doesnt exists");
                    // elem.push(element_json)
                }
            }
            status::Accepted(Some(format!("{}", element_json)))
        }
        _ => {
            status::Accepted(Some(format!("{}",
                          String::from("{'status': 'fail', 'message': 'Not Implemented'}"))))
        }
    }
}

#[put("/api/update/<field>/<io_id>/<index>/<id>", data = "<payload>")]
async fn update_item(field: &str, io_id: u32, index: usize, id: u32, payload: String,
               store: &State<Mutex<BTreeMap<u32, Record>>>) -> status::Accepted<String> {
    // get the database
    let user_store = &mut store.lock().unwrap();
    // let clone_store = user_store.clone();
    let tapes = user_store.get(&id).unwrap().tapes.clone();
    let mut message = String::new();
    match field {
        "update_data" => {
            let mut count = 0;
            for t in tapes {
                if t.tape_info.io_id == io_id {
                    // check first if the data index exists
                    match t.tape_data.get(index) {
                        Some(v) => {
                            user_store.get_mut(&id).unwrap().tapes[count]
                                                                .update_data(&payload, index);
                            message = String::from(
                                                "{'status': 'ok', 'message': 'tape updated'}");
                        }
                        None => {
                            message = String::from("Index does not exists");
                        }
                    }
                }
                else {
                    message = String::from("Error: IO doesnt exists");
                }
                count += 1
            }
            status::Accepted(Some(format!("{}", message)))
        }
        _ => {
            status::Accepted(Some(format!("{}",
                          String::from("{'status': 'fail', 'message': 'Not Implemented'}"))))
        }
    }
}

#[delete("/api/delete/<field>/<io_id>/<index>/<id>")]
async fn delete_item(field: &str, io_id: u32, index: usize, id: u32,
                    store: &State<Mutex<BTreeMap<u32, Record>>>) -> status::Accepted<String> {
    let user_store = &mut store.lock().unwrap();
    let tapes = user_store.get(&id).unwrap().tapes.clone();
    let mut message = String::new();
    match field {
        "tape" => {
            user_store.get_mut(&id).unwrap().tapes.remove(index);
            status::Accepted(Some(format!("{}",
                                  String::from("{'status': 'ok', 'message': 'tape deleted'}"))))
        }
        "data_one" => {
            // get the io
            // let clone_store = user_store.clone();
            let mut count = 0;
            for t in tapes {
                if t.tape_info.io_id == io_id {
                    match t.tape_data.get(index) {
                        Some(v) => {
                            user_store.get_mut(&id).unwrap().tapes[count].delete(index);
                            message = String::from("{'status': 'ok', 'message': 'data at deleted'}");
                        }
                        None => {
                            message = String::from("Index does not exists");
                        }
                    }
                }
                else {
                    message = String::from("Error: IO doesnt exists");
                }
                count += 1;
            }
            status::Accepted(Some(format!("{}", message)))
        }
        "data_prune" => {
            // prune data from an index
            let mut count = 0;
            for t in tapes {
                if t.tape_info.io_id == io_id {
                    match t.tape_data.get(index) {
                        Some(v) => {
                            user_store.get_mut(&id).unwrap().tapes[count].prune(index);
                            message = String::from(
                                                "{'status': 'ok', 'message': 'data at pruned'}");
                        }
                        None => {
                            message = String::from("Index does not exists");
                        }
                    }
                }
                count += 1;
            }
            status::Accepted(Some(format!("{}", message)))
        }
        _ => {
            status::Accepted(Some(format!("{}",
                          String::from("{'status': 'fail', 'message': 'Not Implemented'}"))))
        }
    }
}

#[launch]
fn rocket() -> _ {
    let store: Mutex<BTreeMap<u32, Record>> = Mutex::new(BTreeMap::new());
    // let item = Item(store);
    rocket::build().manage(store).mount("/", routes! [index, insert_item,
                                                            get_item, update_item, delete_item])

}