use serde::{Serialize, Deserialize};
use serde::de::Unexpected::Str;
use serde_json;

use std::fs;
use std::error::Error;

use std::borrow::BorrowMut;
use std::collections::BTreeMap;
/*
#[derive(Serialize, Deserialize)]
pub struct Store {
    pub records: BTreeMap<u32, Record>,
}

impl Store {

    pub fn init() -> Self{
        let mut records: BTreeMap<u32, Record> = BTreeMap::new();
        Self {
            records
        }
    }

    pub fn add(&self, key: u32, value: Record) -> Self {
        // self.records.insert(key, value);
        let mut records = *self.clone().records;
        records.insert(key, value);
        Self {

        }
    }

    pub fn delete(&mut self, key: u32) {
        self.records.remove(&key);
    }

    fn get(&mut self, key: u32) -> &mut Record{
        let record = self.records.get_mut(&key).unwrap();
        record
    }
}
 */

#[derive(Serialize, Deserialize, Clone)]
pub struct Record {
    pub user_data: UserData,
    pub tapes: Vec<Tape>,
}

impl Record {
    // record constructor
    pub fn new(data: &str) -> Self {
        let user_data: UserData = typed_example(data).unwrap();
        let mut tapes: Vec<Tape> = Vec::new();
        Self {
            user_data,
            tapes,
        }
    }

    pub fn push_tape(&mut self, tape: Tape) {
        let mut tapes = self.tapes.clone();
        tapes.push(tape);
        *self = Record {
            user_data: self.user_data.clone(),
            tapes
        };
    }

}

#[derive(Serialize, Deserialize, Clone)]
pub struct UserData{
    pub uname: String,
    pub lon: f32,
    pub lat: f32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Tape {
    pub tape_info: TapeInfo,
    pub tape_data: Vec<TapeElement>,
}

impl Tape {

    pub fn new(data: &str) -> Self {
        let tape_info: TapeInfo = typed_example(data).unwrap();
        let tape_data: Vec<TapeElement> = Vec::new();
        Self {
            tape_info,
            tape_data,
        }
    }

    // insert data to the tape
    pub fn push_data(&mut self, data: &str) {
        let tape_element : TapeElement = typed_example(data).unwrap();
        let mut tape_data = self.tape_data.clone();
        tape_data.push(tape_element);
        *self = Tape {
            tape_info: self.tape_info.clone(),
            tape_data,
        };
        println!("tl {}", self.tape_data.len());
    }

    pub fn update_data(&mut self, data: &str, index: usize) {
        let tape_element : TapeElement = typed_example(data).unwrap();
        let mut tape_data = self.tape_data.clone();
        // remove from index
        tape_data.remove(index);
        // insert at index
        tape_data.insert(index, tape_element);
        *self = Tape {
            tape_info: self.tape_info.clone(),
            tape_data,
        };
    }

    pub fn delete(&mut self, index: usize) {
        let mut tape_data = self.tape_data.clone();
        tape_data.remove(index);
        *self = Tape {
            tape_info: self.tape_info.clone(),
            tape_data,
        };
    }

    pub fn prune(&mut self, index: usize) {
        // prune everything from the given index value
        let tape_data = self.tape_data.clone()[0..index+1].to_vec();
        // let new_tape_data = tape_data[0..index].to_vec();
        *self = Tape {
            tape_info: self.tape_info.clone(),
            tape_data,
        };
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct TapeInfo {
    pub project_id: u32,
    pub io_id: u32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct TapeElement {
    pub step_id: u32,
    pub question_number: u32,
    pub answer: String,
}

macro_rules! struct_fields {
    ($name:expr, $key:ident) => {
        $name.$key
    }
}
fn get_all_records(file_path: String) -> Result<Vec<String>, Box<dyn Error>> {
    // read all json files in a given folder
    let paths = fs::read_dir(file_path).unwrap();

    // hold the filenames in a vec
    let mut file_list: Vec<String> = Vec::new();

    // print the read paths
    for path in paths {
        file_list.push(path.unwrap().path().to_str().unwrap().to_owned());
        //println!("File path: {}", path.unwrap().path().display());
    }
    /*
    if file_list.len() < 0 {
        return Err("List length must not be negative")
    }

     */
    Ok(file_list)
}

fn run(file_name: &String) -> Result<String, Box<dyn Error>> {
    // open the file
    let contents = fs::read_to_string(file_name)
        .expect("Error: Something went wrong");
    // println!("With text: \n{}", contents);
    Ok(contents)
}

fn typed_example <'a, T> (data: &'a str) -> serde_json::Result<(T)>
    where
        T: serde::Deserialize<'a>
{

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let p: T = serde_json::from_str(data)?;

    // Do things just like with any other Rust data structure.
    // let uname = &p.uname;
    // let project_id = p.project_id;
    // let project_id = &p.record_list.get(0).unwrap().project_id;
    // println!("Record data: {}, {}", uname, project_id);

    Ok(p)
}