#[macro_use] extern crate rocket;
use rocket::serde::{Deserialize};
use rocket::http::Status;
use rocket::response::{content, status};
use rocket::http::ext::IntoCollection;
use rocket::{State};
use rocket::request::{self, Request, FromRequest};
use rocket::data::Outcome;

use async_trait::async_trait;

use dbserver::db::db_record::{Record};

use std::collections::BTreeMap;
use std::sync::Mutex;
use std::ops::Deref;
use rocket::outcome::IntoOutcome;

// use serde::Deserialize;
/*
#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

 */
struct Store {
    records: BTreeMap<u32, Record>,
}

/*
impl Store {
    pub fn init() -> Self{
        Self
    }
}
*/

struct Item<'r>(&'r Mutex<&'r Store>);

#[async_trait]
impl<'r> FromRequest<'r> for Item<'r> {
    type Error = ();

     async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, ()>{
        //request.guard::State
        let store = request.rocket().state::<Mutex<&'r Store>>()
            .map(|s| Item(&s)).or_forward(());//Store::init();
        // request::Outcome::Success(store)
         store
    }
}


#[post("/api/makeRecord", data = "<user_input>")]
fn make_record(user_input:  &str, item: &'static State<Item>)
                                                    // -> status::Custom<content::Json<&'static str>> {
                                                    -> status::Accepted<String> {
    // println!("print test {:?}", user_input.clone());
    // let  input:&'static str = user_input;
    //status::Custom(Status::Ok, content::Json("{ \"hi\": \"world\"}"))
    // put the data to a record
    let d = user_input.clone().split(",");
    let lines = d.collect::<Vec<&str>>();
    let payload: String = lines[1..].to_vec().into_iter().collect();
    // get the record struct of the payload
    let record = Record::new(&payload);
    // println!("{}", payload);
    let id = lines.get(0).unwrap().parse::<u32>().unwrap();
    let mut data = item.0;
    // data.records.insert(id, record);
    /*
    let user: &Record = data.get(&id).unwrap();
    let test_display = &user.user_data.uname;

    println!("{}", test_display);
     */
    //println!("{}", id.clone());

    // update the btree
    // store.add()
    /*
    for (i, s) in d.enumerate() {
        println!("{}, {}", s, i);
    }
     */
    // println!("{}", d);
    //let payload =
    // let record = db_record::Record::new(&user_input);
    // let elem = record.user_data.uname;
    // println!("{}", elem);
    // let message = String::from("{success: true}");
    // status::Custom(Status::Ok, content::Json(&message))
    status::Accepted(Some(format!("got it")))
}

/*
#[rocket::main]
async fn main() {
    let _ = rocket::build().mount("/", routes![make_record]).launch().await;
}
 */

#[launch]
fn rocket() -> _ {
    let my_store: &'static Store = &Store {
        records: BTreeMap::new(),
    };
    let store: &'static Mutex<Store> = &Mutex::new(my_store);
    let item = Item(&store);
    rocket::build().manage(item).mount("/", routes![make_record])
}

